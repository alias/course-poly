# Polyhedral Compilation: A Short Survey

_academic year 2023-24_

[Christophe Alias](http://perso.ens-lyon.fr/christophe.alias/index_en.html)

# Goals

The polyhedral model is a powerful framework to develop
state-of-the-art code optimizations and was applied successfully to
automatic parallelization of loop kernels for high-performance and
embedded computing. This seminar will outline the general
principles of the polyhedral model and a selection of important
algorithms behind a polyhedral compiler.

# Planning

* [Handout (to print)](supports/handout.pdf)

* [Lecture 1: Polyhedral Model: Principles and Use-Cases](supports/lecture1-polyhedral-model.pdf)

* [Lecture 2: Polyhedral Scheduling](supports/lecture2-scheduling.pdf)

* [Lecture 3: Polyhedral Loop Tiling](supports/lecture3-tiling.pdf)

* [Lecture 4: Polyhedral Data Layout](supports/lecture4-data-layout.pdf)

# Resources

* [iscc](resources/iscc.tgz) [[online version]](https://compsys-tools.ens-lyon.fr/iscc/index.php)
* [fkcc](https://foobar.ens-lyon.fr/fkcc/) 